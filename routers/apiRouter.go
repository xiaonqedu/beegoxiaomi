package routers

import (
	"beegoxiaomi/controllers/api"

	"github.com/astaxie/beego"
)

func init() {
	ns1 :=
		beego.NewNamespace("/api/v1",
			beego.NSRouter("/", &api.V1Controller{}),
			beego.NSRouter("/nav", &api.V1Controller{}, "get:Nav"),
			beego.NSRouter("/doLogin", &api.V1Controller{}, "post:DoLogin"),
			beego.NSRouter("/doEdit", &api.V1Controller{}, "put:DoEdit"),
			beego.NSRouter("/deleteNav", &api.V1Controller{}, "delete:DeleteNav"),
			beego.NSRouter("/setUser", &api.V1Controller{}, "get:SetUser"),
			beego.NSRouter("/getUser", &api.V1Controller{}, "get:GetUser"),
		)
	ns2 :=
		beego.NewNamespace("/api/v2",
			beego.NSRouter("/", &api.V2Controller{}),
		)
	//注册 namespace
	beego.AddNamespace(ns1, ns2)
}
