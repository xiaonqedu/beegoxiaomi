package models

import (
	_ "github.com/jinzhu/gorm"
)

type Nav struct {
	Id        int     `json:"id"`
	Title     string  `json:"title"`
	Link      string  `json:"link"`
	Position  int     `json:"position"`
	IsOpennew int     `json:"is_opennew"`
	Relation  string  `json:"relation"`
	Sort      int     `json:"sort"`
	Status    int     `json:"status"`
	AddTime   int     `json:"add_time"`
	GoodsItem []Goods `gorm:"-" json:"goods_item"`
}

func (Nav) TableName() string {
	return "nav"
}
