module beegoxiaomi

go 1.14

require (
	github.com/aliyun/aliyun-oss-go-sdk v2.1.2+incompatible
	github.com/astaxie/beego v1.12.2
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/gomarkdown/markdown v0.0.0-20200609195525-3f9352745725
	github.com/hunterhug/go_image v0.0.0-20190710020854-8922226c5f4b
	github.com/jinzhu/gorm v1.9.12
	github.com/objcoding/wxpay v1.0.6
	github.com/olivere/elastic/v7 v7.0.19
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/skip2/go-qrcode v0.0.0-20200526175731-7ac0b40b2038
	github.com/smartwalle/alipay/v3 v3.1.5
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/time v0.0.0-20200416051211-89c76fbcd5d1 // indirect
)
