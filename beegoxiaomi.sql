/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : beegoxiaomi

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 21/08/2020 12:40:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for access
-- ----------------------------
DROP TABLE IF EXISTS `access`;
CREATE TABLE `access`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `type` tinyint(1) NULL DEFAULT NULL,
  `action_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `module_id` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of access
-- ----------------------------
INSERT INTO `access` VALUES (37, '商品管理', 3, '商品类型属性管理', '/goodsTypeAttr', 96, 100, '', 1582191358, 1);
INSERT INTO `access` VALUES (38, '商品管理', 3, '增加商品类型属性', '/goodsTypeAttr/add', 96, 100, '', 1582191781, 1);
INSERT INTO `access` VALUES (39, '商品管理', 3, '修改商品类型属性', '/goodsTypeAttr/edit', 96, 100, '', 1582191797, 1);
INSERT INTO `access` VALUES (40, '商品管理', 3, '执行增加 商品类型属性', '/goodsTypeAttr/doAdd', 96, 100, '', 1582191814, 1);
INSERT INTO `access` VALUES (41, '商品管理', 3, '执行修改 商品类型属性', '/goodsTypeAttr/doEdit', 96, 100, '', 1582191826, 1);
INSERT INTO `access` VALUES (52, '管理员管理', 1, '', '', 0, 1000, '管理员管理', 0, 1);
INSERT INTO `access` VALUES (53, '角色管理', 1, '', '', 0, 999, '角色管理', 0, 1);
INSERT INTO `access` VALUES (54, '管理员管理', 2, '管理员列表', '/manager', 52, 100, '管理员列表', 0, 1);
INSERT INTO `access` VALUES (55, '管理员管理', 2, '增加管理员', '/manager/add', 52, 100, '管理员列表', 0, 1);
INSERT INTO `access` VALUES (56, '管理员管理', 3, '编辑管理员', '/manager/edit', 52, 100, '编辑管理员', 0, 1);
INSERT INTO `access` VALUES (57, '管理员管理', 3, '删除管理员', '/manager/delete', 52, 100, '删除管理员', 0, 1);
INSERT INTO `access` VALUES (59, '角色管理', 2, '角色列表', '/role', 53, 100, '角色列表', 0, 1);
INSERT INTO `access` VALUES (60, '角色管理', 2, '增加角色', '/role/add', 53, 100, '增加角色', 0, 1);
INSERT INTO `access` VALUES (64, '权限管理', 2, '权限列表', '/access', 74, 100, '', 0, 1);
INSERT INTO `access` VALUES (65, '权限管理', 2, '增加权限', '/access/add', 74, 1002, '增加权限', 0, 1);
INSERT INTO `access` VALUES (72, '角色管理', 3, '编辑角色', '/role/edit', 53, 100, '编辑角色', 0, 1);
INSERT INTO `access` VALUES (73, '角色管理', 3, '删除角色', '/role/delete', 53, 100, '删除角色', 0, 1);
INSERT INTO `access` VALUES (74, '权限管理', 1, '', '', 0, 998, '权限管理', 0, 1);
INSERT INTO `access` VALUES (78, '管理员管理', 3, '执行增加', '/manager/doAdd', 52, 100, '执行增加', 0, 1);
INSERT INTO `access` VALUES (79, '管理员管理', 3, '执行修改', '/manager/doEdit', 52, 100, '', 0, 1);
INSERT INTO `access` VALUES (80, '角色管理', 3, '执行修改', '/role/doEdit', 53, 100, '', 0, 1);
INSERT INTO `access` VALUES (81, '角色管理', 3, '执行增加', '/role/doAdd', 53, 100, '', 0, 1);
INSERT INTO `access` VALUES (82, '角色管理', 3, '授权', '/role/auth', 53, 100, '', 0, 1);
INSERT INTO `access` VALUES (83, '角色管理', 3, '执行授权', '/role/doAuth', 53, 100, '', 0, 1);
INSERT INTO `access` VALUES (84, '角色管理', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `access` VALUES (85, '权限管理', 3, '修改权限', '/access/edit', 74, 100, '修改权限', 0, 1);
INSERT INTO `access` VALUES (86, '权限管理', 3, '执行增加', '/access/doAdd', 74, 100, '', 0, 1);
INSERT INTO `access` VALUES (87, '权限管理', 3, '执行修改', '/access/doEdit', 74, 100, '', 0, 1);
INSERT INTO `access` VALUES (88, '权限管理', 3, '删除权限', '/access/delete', 74, 100, '', 0, 1);
INSERT INTO `access` VALUES (89, '轮播图管理', 1, '', '', 0, 111, '轮播图管理', 0, 1);
INSERT INTO `access` VALUES (90, '轮播图管理', 2, '轮播图列表', '/focus', 89, 100, '', 0, 1);
INSERT INTO `access` VALUES (91, '轮播图管理', 2, '增加轮播图', '/focus/add', 89, 100, '增加轮播图', 0, 1);
INSERT INTO `access` VALUES (92, '轮播图管理', 3, '修改轮播图', '/focus/edit', 89, 100, '执行修改', 0, 1);
INSERT INTO `access` VALUES (93, '轮播图管理', 3, '执行增加', '/focus/doAdd', 89, 100, '执行增加', 0, 1);
INSERT INTO `access` VALUES (94, '轮播图管理', 3, '执行修改', '/focus/doEdit', 89, 100, '执行修改', 0, 1);
INSERT INTO `access` VALUES (95, '轮播图管理', 3, '执行删除', '/focus/delete', 89, 100, '执行删除', 0, 1);
INSERT INTO `access` VALUES (96, '商品管理', 1, '', '', 0, 100, '', 0, 1);
INSERT INTO `access` VALUES (97, '商品管理', 2, '商品列表', '/goods', 96, 100, '', 0, 1);
INSERT INTO `access` VALUES (98, '商品管理', 2, '商品分类', '/goodsCate', 96, 100, '商品分类', 0, 1);
INSERT INTO `access` VALUES (99, '商品管理', 3, '增加商品分类', '/goodsCate/add', 96, 100, '增加商品分类', 0, 1);
INSERT INTO `access` VALUES (100, '商品管理', 3, '修改商品分类', '/goodsCate/edit', 96, 100, '修改商品分类', 0, 1);
INSERT INTO `access` VALUES (101, '商品管理', 3, '执行增加商品分类', '/goodsCate/doAdd', 96, 100, '执行增加', 0, 1);
INSERT INTO `access` VALUES (102, '商品管理', 3, '执行修改商品分类', '/goodsCate/doEdit', 96, 100, '执行修改', 0, 1);
INSERT INTO `access` VALUES (103, '商品管理', 3, '删除商品分类', '/goodsCate/delete', 96, 100, '删除商品分类', 0, 1);
INSERT INTO `access` VALUES (104, '商品管理', 2, '商品类型', '/goodsType', 96, 100, '', 0, 1);
INSERT INTO `access` VALUES (105, '商品管理', 3, '增加商品类型', '/goodsType/add', 96, 100, '', 0, 1);
INSERT INTO `access` VALUES (106, '商品管理', 3, '商品类型-执行增加', '/goodsType/doAdd', 96, 100, '', 0, 1);
INSERT INTO `access` VALUES (108, '商品管理', 3, '修改商品类型', '/goodsType/edit', 96, 100, '', 0, 1);
INSERT INTO `access` VALUES (109, '商品管理', 3, '删除商品类型', '/goodsType/delete', 96, 100, '', 0, 1);
INSERT INTO `access` VALUES (110, '商品管理', 3, '商品类型-执行修改', '/goodsType/doEdit', 96, 100, '执行修改', 0, 1);
INSERT INTO `access` VALUES (111, '商品管理', 3, '增加商品', '/goods/add', 96, 100, '增加商品', 0, 1);
INSERT INTO `access` VALUES (112, '商品管理', 3, '修改商品', '/goods/edit', 96, 100, '修改商品', 0, 1);
INSERT INTO `access` VALUES (113, '商品管理', 3, '删除商品', '/goods/delete', 96, 100, '删除商品', 0, 1);
INSERT INTO `access` VALUES (114, '商品管理', 3, '执行-增加商品', '/goods/doAdd', 96, 100, '执行增加商品', 0, 1);
INSERT INTO `access` VALUES (115, '商品管理', 3, '执行-修改商品', '/goods/doEdit', 96, 100, '执行-修改商品', 0, 1);
INSERT INTO `access` VALUES (116, '其他配置', 1, '', '', 0, 100, '', 0, 1);
INSERT INTO `access` VALUES (117, '其他配置', 2, '导航管理', '/nav', 116, 100, '导航管理', 0, 1);
INSERT INTO `access` VALUES (118, '其他配置', 3, '增加导航', '/nav/add', 116, 100, '/nav/add', 0, 1);
INSERT INTO `access` VALUES (119, '其他配置', 3, '修改导航', '/nav/edit', 116, 100, '', 0, 1);
INSERT INTO `access` VALUES (120, '其他配置', 3, '导航-执行增加', '/nav/doAdd', 116, 100, '', 0, 1);
INSERT INTO `access` VALUES (121, '其他配置', 3, '导航-执行修改', '/nav/doEdit', 116, 100, '', 0, 1);
INSERT INTO `access` VALUES (122, '其他配置', 3, '删除导航', '/nav/delete', 116, 100, '', 0, 1);
INSERT INTO `access` VALUES (123, '其他配置', 2, '商店设置', '/setting', 116, 100, '', 0, 1);
INSERT INTO `access` VALUES (124, '其他配置', 3, '商店设置-执行修改', '/setting/doEdit', 116, 100, '', 0, 1);

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` int(0) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `default_address` tinyint(1) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `zipcode` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES (29, 6, '李四1222', '15201686411', '北京市海淀区', 0, 0, '10000');
INSERT INTO `address` VALUES (30, 6, '王五1', '15201686641', '深圳市 宝安区', 0, 0, '10000');
INSERT INTO `address` VALUES (31, 6, '赵四', '15264111111', '北京市海淀区', 0, 0, '10000');
INSERT INTO `address` VALUES (32, 6, '哈哈', '15201686411', '北京市海淀区111', 1, 0, '10000');
INSERT INTO `address` VALUES (33, 6, '王麻子222', '16212345622', '北京市海淀区222', 0, 0, '111222');
INSERT INTO `address` VALUES (34, 9, 'long zhang', '15201686411', '北京市海淀区', 1, 0, '10000');
INSERT INTO `address` VALUES (35, 10, 'zhangsan', '15264111521', '北京市海淀区', 1, 0, '10000');

-- ----------------------------
-- Table structure for focus
-- ----------------------------
DROP TABLE IF EXISTS `focus`;
CREATE TABLE `focus`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `focus_type` tinyint(1) NULL DEFAULT NULL,
  `focus_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of focus
-- ----------------------------
INSERT INTO `focus` VALUES (9, 'Redmi Note 9', 1, 'static/upload/20200608/1591582355.jpg', 'http://www.itying.com', 611, 1, 1591535347);
INSERT INTO `focus` VALUES (10, '小米冰箱', 1, 'static/upload/20200707/1594126183364215400.jpg', 'http://www.itying.com', 11233, 1, 1591535381);
INSERT INTO `focus` VALUES (12, '手环', 1, 'static/upload/20200630/1593508415890959500.jpg', 'http://www.itying.com', 1000, 1, 1592315316);

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sub_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `cate_id` int(0) NULL DEFAULT NULL,
  `click_count` int(0) NULL DEFAULT NULL,
  `goods_number` int(0) NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `market_price` decimal(10, 2) NULL DEFAULT NULL,
  `relation_goods` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_attr` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_gift` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_fitting` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `is_delete` tinyint(0) NULL DEFAULT NULL,
  `is_hot` tinyint(0) NULL DEFAULT NULL,
  `is_best` tinyint(0) NULL DEFAULT NULL,
  `is_new` tinyint(0) NULL DEFAULT NULL,
  `goods_type_id` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `status` tinyint(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (19, '小米9', '火爆热卖中，6GB+64GB/6GB+128GB闪降100元，到手价1299元起', '', 1, 100, 1000, 1299.00, 1441.00, '1,2', '尺寸:41,42,43', '2,5', '8GB+256GB', 'static/upload/20200617/1592392307796676500.jpg', '1,2', '1,2', '', '', '<p>火爆热卖中，6GB+64GB/6GB+128GB闪降100元，到手价1299元起</p>', 0, 1, 1, 1, 3, 100, 1, 1592392307);
INSERT INTO `goods` VALUES (20, '小米11', '火爆热卖中，6GB+64GB/6GB+128GB闪降100元，到手价1299元起', '', 1, 100, 0, 124124.00, 124.00, '1', '4', '3,5', '12G+512GB', 'static/upload/20200630/1593488970817764900.jpg', '2', '3', '', '', '<p>火爆热卖中，6GB+64GB/6GB+128GB闪降100元，到手价1299元起</p>', 0, 1, 1, 0, 3, 0, 1, 1592392495);
INSERT INTO `goods` VALUES (21, '小米8年度旗舰222', '火爆热卖中，6GB+64GB/6GB+128GB闪降100元，到手价1299元起', '', 1, 100, 1000, 1112.00, 1113.00, '1,2', '1,2', '2,3,4,5', '3GB+32GB', 'static/upload/20200618/1592452490474942700.jpg', '1,2', '1,2', '1,2', '1,2', '<p>火爆热卖中，6GB+64GB/6GB+128GB闪降100元，到手价1299元起</p>', 0, 1, 0, 1, 1, 11111, 1, 1592392825);
INSERT INTO `goods` VALUES (22, 'Redmi 7A', '「3GB+32GB到手价仅549元」4000mAh超长续航 / 骁龙8核处理器 / 标配10W快充 / AI人脸解锁 / 大字体，大音量，无线收音机 / 整机生活防泼溅 / 极简模式，亲情守护', '', 2, 100, 1000, 549.00, 799.00, '', '', '3,4', '3GB+32GB', 'static/upload/20200622/1592820040.jpg', '', '', '', '', '<p><span style=\"color: rgb(51, 51, 51); font-family: F9ab65; font-size: 10.4922px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">小巧机身蕴藏4000mAh大电量，配合MIUI系统级省电优化，精细调控，从此告别电量焦虑，尽情尽欢！</span></p>', 0, 1, 1, 0, 1, 100, 1, 1592820016);
INSERT INTO `goods` VALUES (23, 'Redmi 智能电视 X65', '全金属边框/4K超高清/MEMC运动补偿/8单元重低音音响系统', '', 5, 100, 1000, 2999.00, 3299.00, '', '', '4', '56寸', 'static/upload/20200622/1592820111.jpg', '', '', '', '', '<p><span style=\'color: rgb(176, 176, 176); font-family: \"Helvetica Neue\", Helvetica, Arial, \"Microsoft Yahei\", \"Hiragino Sans GB\", \"Heiti SC\", \"WenQuanYi Micro Hei\", sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\'>全金属边框/4K超高清/MEMC运动补偿/8单元重低音音响系统</span> </p>', 0, 1, 0, 0, 0, 100, 1, 1592820111);
INSERT INTO `goods` VALUES (24, 'RedmiBook 13 全面屏', '四窄边全面屏 / 全新十代酷睿™处理器 / 全金属超轻机身 / MX250 高性能独显 / 小米互传 / 专业「飓风」散热系统 / 11小时长续航', '', 5, 100, 1000, 4499.00, 4799.00, '', '', '4,5', '8G+128G', 'static/upload/20200622/1592820244.jpg', '', '', '', '', '<p><span style=\'color: rgb(176, 176, 176); font-family: \"Helvetica Neue\", Helvetica, Arial, \"Microsoft Yahei\", \"Hiragino Sans GB\", \"Heiti SC\", \"WenQuanYi Micro Hei\", sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\'>四窄边全面屏 / 全新十代酷睿&trade;处理器 / 全金属超轻机身 / MX250 高性能独显 / 小米互传 / 专业「飓风」散热系统 / 11小时长续航</span></p>', 0, 1, 1, 0, 0, 100, 1, 1592820244);
INSERT INTO `goods` VALUES (25, '米家电磁炉333', '99挡微调控火 / 支持低温烹饪 / 100+烹饪模式', '', 3, 100, 1000, 299.00, 399.00, '', '', '', '', 'static/upload/20200622/1592820331.jpg', '', '', '', '', '<p>米家电磁炉</p>', 0, 1, 0, 0, 0, 100, 1, 1592820331);
INSERT INTO `goods` VALUES (28, '腾讯黑鲨游戏手机3', '骁龙865处理器 / 双模5G / 腾讯Solar Core游戏引擎 / 270Hz触控采样率+90Hz屏幕刷新率 / 最高65W极速闪充+背部磁吸充电 / 4720mAh大容量双电池 / UFS3.0闪存 / “三明治”液冷散热 / 屏幕压感3.0 / 游戏语音操控', '', 1, 100, 1000, 3999.00, 4999.00, '', '', '', '', 'static/upload/20200630/1593488894443396500.jpg', '', '', '', '', '', 0, 1, 0, 0, 0, 10, 1, 1593488894);
INSERT INTO `goods` VALUES (29, 'Redmi Note 8', 'Redmi Note 8 降价了', '', 3, 100, 1100, 949.00, 1049.00, '', '', '', '', 'static/upload/20200707/1594090379813369400.jpg', '', '', '', '', '', 0, 1, 1, 0, 0, 10, 1, 1593491318);
INSERT INTO `goods` VALUES (30, '小米30', '', '', 21, 100, 100, 100.00, 100.00, '', '', '', '', 'static/upload/20200716/1594901901204635500.jpg', '', '', '', '', '<p>qwrqw</p><p><br></p><p><img src=\"http://bee.apiying.com/static/upload/20200706/1593999345774680300.jpg\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></p>', 0, 0, 0, 0, 0, 0, 1, 1593999389);
INSERT INTO `goods` VALUES (31, '小米CC9', '前置3200万自拍 / 索尼4800万超广角三摄 / 多色炫彩轻薄机身 / 6.39英寸三星 AMOLED水滴屏 /4030mAh大电量 / 骁龙710处理器 / 屏幕指纹 / NFC功能 / 红外遥控功能 / Game Turbo2.0游戏加速', '', 1, 100, 0, 0.00, 0.00, '', '', '2,3', '6G+64G', 'static/upload/20200707/1594090045924272100.jpg', '', '', '', '', '<p>暗夜王子</p><p>银灰光泽流转于玻璃机身，层层递进，既简约神秘，</p><p>又不失年轻活力</p><h2 style=\"font-size: 0.84rem; margin: 0px; font-family: Fe73db; padding: 0px; font-weight: normal; line-height: 1.1rem; color: rgb(0, 0, 0); transform: translate3d(0px, 0px, 0px); opacity: 1; transition: transform 700ms cubic-bezier(0.25, 0.46, 0.33, 0.98) 0.3s, opacity 700ms cubic-bezier(0.25, 0.46, 0.33, 0.98); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">179g 超轻手感</h2><h2 style=\"font-size: 0.84rem; margin: 0px; font-family: Fe73db; padding: 0px; font-weight: normal; line-height: 1.1rem; color: rgb(0, 0, 0); transform: translate3d(0px, 0px, 0px); opacity: 1; transition: transform 700ms cubic-bezier(0.25, 0.46, 0.33, 0.98) 0.3s, opacity 700ms cubic-bezier(0.25, 0.46, 0.33, 0.98); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"><br>4030mAh 超大电量</h2><p><br></p><p><br></p>', 0, 0, 0, 0, 1, 0, 1, 1594090045);
INSERT INTO `goods` VALUES (32, '小米MIX Alpha', '创新环绕屏，极具未来感的智能交互体验 / 1亿像素超高清相机，超大感光元件 / 5G双卡全网通超高速网络 / 骁龙855Plus旗舰处理器 / 纳米硅基锂离子4050mAh电池，40W超级快充 / 钛合金+精密陶瓷+蓝宝石镜片前沿工艺', '', 1, 100, 1000, 1899.00, 1999.00, '', '', '2,3', '', 'static/upload/20200707/1594090301926914600.jpg', '', '', '', '', '<p><span style=\'color: rgb(176, 176, 176); font-family: \"Helvetica Neue\", Helvetica, Arial, \"Microsoft Yahei\", \"Hiragino Sans GB\", \"Heiti SC\", \"WenQuanYi Micro Hei\", sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\'>创新环绕屏，极具未来感的智能交互体验 / 1亿像素超高清相机，超大感光元件 / 5G双卡全网通超高速网络 / 骁龙855Plus旗舰处理器 / 纳米硅基锂离子4050mAh电池，40W超级快充 / 钛合金+精密陶瓷+蓝宝石镜片前沿工艺</span></p>', 0, 1, 0, 0, 1, 1000, 1, 1594090301);
INSERT INTO `goods` VALUES (33, 'Redmi K30 系列-Redmi K30 4G', '双模5G / 三路并发 / 7nm 5G低功耗处理器 / 120Hz高帧率流速屏 / 6.67\'\'小孔径全面屏 / 索尼6400万前后六摄 / 最高可选8GB+256GB大存储 / 4500mAh+30W快充 / 3D四曲面玻璃机身 / 多功能NFC', '', 3, 100, 0, 1599.00, 1899.00, '33,34', '', '6,7', 'Redmi K30 4G', 'static/upload/20200717/1594971710634525600.jpg', '', '', '', '', '<p><span style=\"font-size: 36px;\">模5G / 三路并发 / 7nm 5G低功耗处理器&nbsp;</span></p><p><br></p><p><br></p><p>&nbsp;120Hz高帧率流速屏 / 6.67&#39;&#39;小孔径全面屏 / 索尼6400万前后六摄 / 最高可选8GB+256GB大存储 / 4500mAh+30W快充 / 3D四曲面玻璃机身 / 多功能NFC</p><p><br></p><p><br><img src=\"/static/upload/20200718/1595041528401806900.jpg\" style=\"width: 390px;\" class=\"fr-fic fr-dib\" alt=\"beego\"></p><p class=\"fr-img-space-wrap2\">这是一个内容</p><p class=\"fr-img-space-wrap2\"><br></p><p class=\"fr-img-space-wrap2\"><img src=\"/static/upload/20200718/1595041554183281500.jpg\" style=\"width: 348px;\" class=\"fr-fic fr-dib\"></p><p class=\"fr-img-space-wrap2\">&nbsp;</p><p><br></p><p><br></p><p class=\"fr-img-space-wrap2\">&nbsp;</p><p><br></p><p><br></p>', 0, 1, 1, 0, 1, 1001, 1, 1594971710);
INSERT INTO `goods` VALUES (34, 'Redmi K30 系列-Redmi K30 5G', '120Hz流速屏 前置挖孔双摄 索尼6400万后置四摄 4500mAh超长续航 27W快充 6GB+128GB 深海微光 游戏智能手机', '', 3, 100, 0, 2399.00, 2599.00, '33,34', '', '6,7,8', 'Redmi K30 5G', 'static/upload/20200717/1594973579432414700.jpg', '', '', '', '', '<p><strong><span style=\"font-size: 36px;\">双模5G / 三路并发 / 7nm 5G低功耗处理器 </span></strong></p><p><br></p><p><br></p><p>&nbsp;120Hz高帧率流速屏 / 6.67&#39;&#39;小孔径全面屏 / 索尼6400万前后六摄 / 最高可选8GB+256GB大存储 / 4500mAh+30W快充 / 3D四曲面玻璃机身 / 多功能NFC</p><p><br></p><p><br></p>', 0, 0, 0, 0, 1, 1001, 1, 1594973579);

-- ----------------------------
-- Table structure for goods_attr
-- ----------------------------
DROP TABLE IF EXISTS `goods_attr`;
CREATE TABLE `goods_attr`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `goods_id` int(0) NULL DEFAULT NULL,
  `attribute_cate_id` int(0) NULL DEFAULT NULL,
  `attribute_id` int(0) NULL DEFAULT NULL,
  `attribute_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attribute_type` tinyint(1) NULL DEFAULT NULL,
  `attribute_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 243 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_attr
-- ----------------------------
INSERT INTO `goods_attr` VALUES (28, 18, 0, 0, '', 0, '硬盘1', 10, 1592391786, 1);
INSERT INTO `goods_attr` VALUES (29, 18, 0, 0, '', 0, '内存1', 10, 1592391786, 1);
INSERT INTO `goods_attr` VALUES (30, 18, 0, 0, '', 0, '14寸\r\n', 10, 1592391786, 1);
INSERT INTO `goods_attr` VALUES (64, 21, 1, 1, '基本信息', 1, '基本信息1', 10, 1592452499, 1);
INSERT INTO `goods_attr` VALUES (65, 21, 1, 7, '性能	', 2, '性能1', 10, 1592452499, 1);
INSERT INTO `goods_attr` VALUES (66, 21, 1, 8, '相机', 2, '相机1', 10, 1592452499, 1);
INSERT INTO `goods_attr` VALUES (67, 21, 1, 9, '支持蓝牙', 3, '是\r\n', 10, 1592452499, 1);
INSERT INTO `goods_attr` VALUES (68, 21, 1, 10, '屏幕', 2, '屏', 10, 1592452499, 1);
INSERT INTO `goods_attr` VALUES (75, 31, 1, 1, '基本信息', 1, '', 10, 1594090045, 1);
INSERT INTO `goods_attr` VALUES (76, 31, 1, 7, '性能	', 2, '骁龙 710\r\n八核处理器', 10, 1594090045, 1);
INSERT INTO `goods_attr` VALUES (77, 31, 1, 8, '相机', 2, '', 10, 1594090045, 1);
INSERT INTO `goods_attr` VALUES (78, 31, 1, 9, '支持蓝牙', 3, '是\r\n', 10, 1594090045, 1);
INSERT INTO `goods_attr` VALUES (79, 31, 1, 10, '屏幕', 2, '\r\n6.39英寸\r\n三星 AMOLED 屏幕\r\n德国 VDE 低蓝光护眼认证', 10, 1594090045, 1);
INSERT INTO `goods_attr` VALUES (80, 20, 3, 14, '硬盘', 1, '硬盘6666666666', 10, 1594090055, 1);
INSERT INTO `goods_attr` VALUES (81, 20, 3, 15, '内存', 1, '内存666666666', 10, 1594090055, 1);
INSERT INTO `goods_attr` VALUES (82, 20, 3, 16, '尺寸', 3, '14寸\r\n', 10, 1594090055, 1);
INSERT INTO `goods_attr` VALUES (93, 32, 1, 1, '基本信息', 1, '', 10, 1594971742, 1);
INSERT INTO `goods_attr` VALUES (94, 32, 1, 7, '性能	', 2, 'MIUI Alpha', 10, 1594971742, 1);
INSERT INTO `goods_attr` VALUES (95, 32, 1, 8, '相机', 2, '1亿像素超旗舰三摄', 10, 1594971742, 1);
INSERT INTO `goods_attr` VALUES (96, 32, 1, 9, '支持蓝牙', 3, '是\r\n', 10, 1594971742, 1);
INSERT INTO `goods_attr` VALUES (97, 32, 1, 10, '屏幕', 2, '7.92 英寸 柔性 OLED 屏幕', 10, 1594971742, 1);
INSERT INTO `goods_attr` VALUES (193, 34, 1, 1, '基本信息', 1, '小米（MI）', 10, 1595219478, 1);
INSERT INTO `goods_attr` VALUES (194, 34, 1, 7, '性能	', 2, '高通(Qualcomm)', 10, 1595219478, 1);
INSERT INTO `goods_attr` VALUES (195, 34, 1, 8, '相机', 2, '后摄3摄像素\r\n200万像素', 10, 1595219478, 1);
INSERT INTO `goods_attr` VALUES (196, 34, 1, 9, '支持蓝牙', 3, '是\r\n', 10, 1595219478, 1);
INSERT INTO `goods_attr` VALUES (197, 34, 1, 10, '屏幕', 2, '200万像素', 10, 1595219478, 1);
INSERT INTO `goods_attr` VALUES (238, 33, 1, 1, '基本信息', 1, 'Redmi K30-上市年份 2019年-首销日期', 10, 1595220426, 1);
INSERT INTO `goods_attr` VALUES (239, 33, 1, 7, '性能	', 2, '## 6GB + 128GB\r\n* 运行内存：4GB/6GB\r\n* 机身存储：64GB/128GB\r\n* 支持高达 512GB 可扩展存储\r\n* LPDDR4 x 高速内存', 10, 1595220426, 1);
INSERT INTO `goods_attr` VALUES (240, 33, 1, 8, '相机', 2, '### 全场景 AI 四摄\r\n- 1300 万超清主摄\r\n- 800 万超广角相机\r\n- 500 万微距相机\r\n- 200 万人像景深相机\r\n- 后置相机功能\r\nAI 相机 | 电影模式 | 微距视频拍摄 | 短视频录制 | 视频美颜 | 人像模式背景虚化 | 超广角畸变矫正 | 专业模式 | 水平仪 | 连拍模式 | HDR | AI 美颜 | 倒计时拍照 | 万花筒', 10, 1595220426, 1);
INSERT INTO `goods_attr` VALUES (241, 33, 1, 9, '支持蓝牙', 3, '是\r\n', 10, 1595220426, 1);
INSERT INTO `goods_attr` VALUES (242, 33, 1, 10, '屏幕', 2, '', 10, 1595220426, 1);

-- ----------------------------
-- Table structure for goods_cate
-- ----------------------------
DROP TABLE IF EXISTS `goods_cate`;
CREATE TABLE `goods_cate`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cate_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `template` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pid` int(0) NULL DEFAULT NULL,
  `sub_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_cate
-- ----------------------------
INSERT INTO `goods_cate` VALUES (1, '手机 电话卡', '', '', '', 0, '手机', '手机', '手机', 1, 10, 1582461745);
INSERT INTO `goods_cate` VALUES (2, '小米10 Pro', 'static/upload/2020223/1582463294.png', '', '', 1, '小米10 Pro', '小米10 Pro', '小米10 Pro', 1, 1, 1582463294);
INSERT INTO `goods_cate` VALUES (3, 'Redmi 8', 'static/upload/2020223/1582463357.png', '', 'itying/product/aaa.html', 1, 'Redmi 8 11', 'Redmi 8 111', 'Redmi 8 111', 1, 2, 1582463357);
INSERT INTO `goods_cate` VALUES (4, '电视 盒子', '', '', '', 0, '电视 盒子', '电视 盒子', '电视 盒子', 1, 9, 1582463515);
INSERT INTO `goods_cate` VALUES (5, '小米电视5 55英寸', 'static/upload/2020223/1582464603.png', '', '', 4, '小米电视5 55英寸', '小米电视5 55英寸', '小米电视5 55英寸', 1, 0, 1582464603);
INSERT INTO `goods_cate` VALUES (6, '家电 插线板', '', '', '', 0, '', '', '', 1, 0, 1582513219);
INSERT INTO `goods_cate` VALUES (7, '出行 穿戴', '', '', '', 0, '', '', '', 1, 0, 1582513235);
INSERT INTO `goods_cate` VALUES (8, '智能 路由器', '', '', '', 0, '', '', '', 1, 0, 1582513270);
INSERT INTO `goods_cate` VALUES (9, '电源 配件', '', '', '', 0, '', '', '', 1, 0, 1582513285);
INSERT INTO `goods_cate` VALUES (10, '健康 儿童', '', '', '', 0, '', '', '', 1, 0, 1582513300);
INSERT INTO `goods_cate` VALUES (11, '耳机 音响', '', '', '', 0, '', '', '', 1, 0, 1582513338);
INSERT INTO `goods_cate` VALUES (12, '生活 箱包', '', '', '', 0, '', '', '', 1, 0, 1582513349);
INSERT INTO `goods_cate` VALUES (13, '冰箱', 'static/upload/2020224/1582513945.jpg', '', '', 6, '冰箱', '冰箱', '冰箱', 1, 0, 1582513945);
INSERT INTO `goods_cate` VALUES (14, '微波炉', 'static/upload/2020224/1582514001.jpg', '', '', 6, '', '', '', 1, 0, 1582513960);
INSERT INTO `goods_cate` VALUES (15, '小米手表', 'static/upload/2020224/1582514113.png', '', '', 7, '小米手表', '小米手表', '小米手表', 1, 0, 1582514113);
INSERT INTO `goods_cate` VALUES (16, '平衡车', 'static/upload/2020224/1582514151.jpg', '', '', 7, '平衡车', '平衡车', '平衡车', 1, 0, 1582514151);
INSERT INTO `goods_cate` VALUES (17, '路由器', 'static/upload/2020224/1582514289.png', '', '', 8, '路由器', '路由器', '路由器', 1, 0, 1582514289);
INSERT INTO `goods_cate` VALUES (18, '摄像机', 'static/upload/2020224/1582514318.jpg', '', '', 8, '摄像机', '摄像机', '摄像机', 1, 0, 1582514318);
INSERT INTO `goods_cate` VALUES (19, '全屏电视55寸', 'static/upload/2020224/1582514664.jpg', '', '', 4, '', '', '', 1, 0, 1582514664);
INSERT INTO `goods_cate` VALUES (20, '移动电源', 'static/upload/2020224/1582514810.png', '', '', 9, '移动电源', '移动电源', '移动电源', 1, 0, 1582514810);
INSERT INTO `goods_cate` VALUES (21, '小米移动 电话卡', 'static/upload/20200707/1594088379260944400.jpg', '', '', 1, '', '', '', 1, 10, 1594088238);
INSERT INTO `goods_cate` VALUES (22, '笔记本 显示器 平板', '', '', '', 0, '', '', '', 1, 8, 1594088325);
INSERT INTO `goods_cate` VALUES (23, '婴儿推车', 'static/upload/20200707/1594089221653126500.jpg', '', '', 10, '', '', '', 1, 10, 1594089221);
INSERT INTO `goods_cate` VALUES (24, '小米小爱音箱', 'static/upload/20200707/1594089287177874300.png', '', '', 11, '', '', '', 1, 10, 1594089287);
INSERT INTO `goods_cate` VALUES (25, '小背包', 'static/upload/20200707/1594089350990524100.jpg', '', '', 12, '', '', '', 1, 10, 1594089350);
INSERT INTO `goods_cate` VALUES (26, '显示器', 'static/upload/20200707/1594089381272256100.png', '', '', 22, '', '', '', 1, 10, 1594089381);

-- ----------------------------
-- Table structure for goods_color
-- ----------------------------
DROP TABLE IF EXISTS `goods_color`;
CREATE TABLE `goods_color`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `color_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_color
-- ----------------------------
INSERT INTO `goods_color` VALUES (1, '红色', 'red', 1);
INSERT INTO `goods_color` VALUES (2, '黑色', '#000', 1);
INSERT INTO `goods_color` VALUES (3, '红色', 'yellow', 1);
INSERT INTO `goods_color` VALUES (4, '金色', '#ebf10f', 1);
INSERT INTO `goods_color` VALUES (5, '灰色', '#eee', 1);
INSERT INTO `goods_color` VALUES (6, '蓝色', 'blue', 1);
INSERT INTO `goods_color` VALUES (7, '紫色', '#B09AFE', 1);
INSERT INTO `goods_color` VALUES (8, '薄荷', '#A0FBE9', 1);

-- ----------------------------
-- Table structure for goods_image
-- ----------------------------
DROP TABLE IF EXISTS `goods_image`;
CREATE TABLE `goods_image`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `goods_id` int(0) NULL DEFAULT NULL,
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `color_id` int(0) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_image
-- ----------------------------
INSERT INTO `goods_image` VALUES (13, 18, '/static/upload/20200617/1592391780837536200.jpg', 0, 10, 1592391786, 1);
INSERT INTO `goods_image` VALUES (14, 18, '/static/upload/20200617/1592391780895539500.jpg', 0, 10, 1592391786, 1);
INSERT INTO `goods_image` VALUES (15, 18, '/static/upload/20200617/1592391780954542900.jpg', 0, 10, 1592391786, 1);
INSERT INTO `goods_image` VALUES (16, 18, '/static/upload/20200617/1592391780954542900.jpg', 0, 0, 0, 0);
INSERT INTO `goods_image` VALUES (17, 18, '/static/upload/20200617/1592392823882194900.jpg', 0, 0, 0, 0);
INSERT INTO `goods_image` VALUES (18, 20, '/static/upload/20200617/1592392494637363200.jpg', 5, 10, 1592392495, 1);
INSERT INTO `goods_image` VALUES (19, 20, '/static/upload/20200617/1592392494693366400.jpg', 3, 10, 1592392495, 1);
INSERT INTO `goods_image` VALUES (20, 21, '/static/upload/20200617/1592392823882194900.jpg', 4, 10, 1592392825, 1);
INSERT INTO `goods_image` VALUES (27, 19, '/static/upload/20200622/1592799749938586100.jpg', 2, 10, 1592799750, 1);
INSERT INTO `goods_image` VALUES (28, 19, '/static/upload/20200622/1592799749994589300.jpg', 2, 10, 1592799750, 1);
INSERT INTO `goods_image` VALUES (29, 19, '/static/upload/20200622/1592799750042592100.jpg', 5, 10, 1592799750, 1);
INSERT INTO `goods_image` VALUES (30, 20, '/static/upload/20200622/1592817126555431600.jpg', 0, 10, 1592817127, 1);
INSERT INTO `goods_image` VALUES (31, 20, '/static/upload/20200622/1592817126614435000.jpg', 0, 10, 1592817127, 1);
INSERT INTO `goods_image` VALUES (32, 20, '/static/upload/20200622/1592817126670438200.jpg', 0, 10, 1592817127, 1);
INSERT INTO `goods_image` VALUES (35, 19, '/static/upload/20200630/1593489163564789400.jpg', 0, 10, 1593489166, 1);
INSERT INTO `goods_image` VALUES (36, 19, '/static/upload/20200630/1593489163617792400.jpg', 0, 10, 1593489166, 1);
INSERT INTO `goods_image` VALUES (37, 30, 'static/upload/20200706/1593999368958006300.jpg', 0, 10, 1593999389, 1);
INSERT INTO `goods_image` VALUES (38, 33, 'static/upload/20200717/1594971708712415700.jpg', 6, 10, 1594971710, 1);
INSERT INTO `goods_image` VALUES (39, 33, 'static/upload/20200717/1594971708761418500.jpg', 6, 10, 1594971710, 1);
INSERT INTO `goods_image` VALUES (40, 33, 'static/upload/20200717/1594971708819421800.jpg', 6, 10, 1594971710, 1);
INSERT INTO `goods_image` VALUES (41, 34, 'static/upload/20200717/1594973578426357200.jpg', 6, 10, 1594973579, 1);
INSERT INTO `goods_image` VALUES (42, 34, 'static/upload/20200717/1594973578471359800.jpg', 6, 10, 1594973579, 1);
INSERT INTO `goods_image` VALUES (43, 34, 'static/upload/20200717/1594973578510362000.jpg', 6, 10, 1594973579, 1);
INSERT INTO `goods_image` VALUES (44, 33, 'static/upload/20200719/1595166087210430600.jpg', 7, 10, 1595166089, 1);
INSERT INTO `goods_image` VALUES (45, 33, 'static/upload/20200719/1595166087263433600.jpg', 7, 10, 1595166089, 1);
INSERT INTO `goods_image` VALUES (46, 33, 'static/upload/20200719/1595166087325437100.jpg', 7, 10, 1595166089, 1);
INSERT INTO `goods_image` VALUES (47, 33, 'static/upload/20200719/1595166087480446000.jpg', 7, 10, 1595166089, 1);
INSERT INTO `goods_image` VALUES (48, 34, 'static/upload/20200719/1595166524584446900.jpg', 8, 10, 1595166526, 1);
INSERT INTO `goods_image` VALUES (49, 34, 'static/upload/20200719/1595166524751456500.jpg', 8, 10, 1595166526, 1);
INSERT INTO `goods_image` VALUES (50, 34, 'static/upload/20200719/1595166524751456500.jpg', 8, 10, 1595166526, 1);
INSERT INTO `goods_image` VALUES (51, 34, 'static/upload/20200719/1595166524934466900.jpg', 7, 10, 1595166526, 1);
INSERT INTO `goods_image` VALUES (52, 34, 'static/upload/20200719/1595166525012471400.jpg', 7, 10, 1595166526, 1);
INSERT INTO `goods_image` VALUES (53, 34, 'static/upload/20200719/1595166525076475100.jpg', 7, 10, 1595166526, 1);
INSERT INTO `goods_image` VALUES (54, 34, 'static/upload/20200719/1595166525189481500.jpg', 7, 10, 1595166526, 1);

-- ----------------------------
-- Table structure for goods_type
-- ----------------------------
DROP TABLE IF EXISTS `goods_type`;
CREATE TABLE `goods_type`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_type
-- ----------------------------
INSERT INTO `goods_type` VALUES (1, '手机', '手机', 1, 1582120088);
INSERT INTO `goods_type` VALUES (2, '电脑', '电脑', 0, 1582120130);
INSERT INTO `goods_type` VALUES (3, '笔记本', '笔记本', 1, 1582120143);
INSERT INTO `goods_type` VALUES (4, '路由器', '路由器', 1, 1591784140);

-- ----------------------------
-- Table structure for goods_type_attribute
-- ----------------------------
DROP TABLE IF EXISTS `goods_type_attribute`;
CREATE TABLE `goods_type_attribute`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `cate_id` int(0) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `attr_type` tinyint(1) NULL DEFAULT NULL,
  `attr_value` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_type_attribute
-- ----------------------------
INSERT INTO `goods_type_attribute` VALUES (1, 1, '基本信息', 1, '', 1, 10, 1582358795);
INSERT INTO `goods_type_attribute` VALUES (2, 2, '主体', 3, '111\r\n1111', 1, 19, 1582361589);
INSERT INTO `goods_type_attribute` VALUES (3, 2, '内存', 1, '', 1, NULL, 1582361598);
INSERT INTO `goods_type_attribute` VALUES (4, 2, '硬盘', 1, '', 1, NULL, 1582361780);
INSERT INTO `goods_type_attribute` VALUES (5, 2, '显示器', 1, '', 1, NULL, 1582361804);
INSERT INTO `goods_type_attribute` VALUES (6, 2, '支持蓝牙1', 3, '是\r\n否', 1, 0, 1582362691);
INSERT INTO `goods_type_attribute` VALUES (7, 1, '性能	', 2, '', 1, 111, 1591844598);
INSERT INTO `goods_type_attribute` VALUES (8, 1, '相机', 2, '', 1, 0, 1591844635);
INSERT INTO `goods_type_attribute` VALUES (9, 1, '支持蓝牙', 3, '是\r\n否', 1, 0, 1591844649);
INSERT INTO `goods_type_attribute` VALUES (10, 1, '屏幕', 2, '', 1, 0, 1591844675);
INSERT INTO `goods_type_attribute` VALUES (11, 2, '外观', 2, '', 1, 0, 1591844688);
INSERT INTO `goods_type_attribute` VALUES (13, 4, '品牌', 1, '', 1, 0, 1591851350);
INSERT INTO `goods_type_attribute` VALUES (14, 3, '硬盘', 1, '', 1, 0, 1591851429);
INSERT INTO `goods_type_attribute` VALUES (15, 3, '内存', 1, '', 1, 0, 1591851442);
INSERT INTO `goods_type_attribute` VALUES (16, 3, '尺寸', 3, '14寸\r\n15寸', 1, 0, 1591851471);

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(0) NULL DEFAULT 1,
  `role_id` int(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `is_super` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '15201686411', '518864@qq.com', 1, 12, 0, 1);
INSERT INTO `manager` VALUES (2, 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', '15201686412', '34233869@qq.com', 1, 2, 1581661532, 0);
INSERT INTO `manager` VALUES (7, 'lisi', 'e10adc3949ba59abbe56e057f20f883e', '15111324214', '3333498@qq.com', 1, 11, 1591078419, 0);

-- ----------------------------
-- Table structure for nav
-- ----------------------------
DROP TABLE IF EXISTS `nav`;
CREATE TABLE `nav`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `position` tinyint(1) NULL DEFAULT NULL,
  `is_opennew` tinyint(1) NULL DEFAULT NULL,
  `relation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sort` int(0) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nav
-- ----------------------------
INSERT INTO `nav` VALUES (1, '小米商城', 'http://www.itying.com', 2, 2, '21,22,23,24', 10, 1, 1592919226);
INSERT INTO `nav` VALUES (2, 'MIUI', 'http://www.itying.com', 1, 1, '1', 10, 1, 1592921999);
INSERT INTO `nav` VALUES (3, '小米手机', 'https://shouji.mi.com/', 2, 2, '19,20', 10, 1, 1592922081);
INSERT INTO `nav` VALUES (4, '小米电视', 'https://ds.mi.com/', 2, 2, '23,24', 10, 1, 1592922273);
INSERT INTO `nav` VALUES (5, '路由器', 'http://bbs.itying.com', 2, 1, '25', 10, 1, 1592922331);
INSERT INTO `nav` VALUES (8, '云服务', 'https://i.mi.com/', 1, 2, '2', 10, 1, 1593529309);
INSERT INTO `nav` VALUES (9, '金融', 'https://jr.mi.com/?from=micom', 1, 1, '1', 10, 1, 1593529329);
INSERT INTO `nav` VALUES (10, '有品', 'https://youpin.mi.com/', 1, 1, '1', 10, 1, 1593529346);
INSERT INTO `nav` VALUES (11, '家电', '', 2, 1, '1', 10, 1, 1593529451);
INSERT INTO `nav` VALUES (12, '智能电视', '', 2, 1, '1', 10, 1, 1593529470);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` int(0) NULL DEFAULT NULL,
  `order_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `all_price` decimal(10, 2) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `zipcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `pay_status` tinyint(1) NULL DEFAULT NULL,
  `pay_type` tinyint(1) NULL DEFAULT NULL,
  `order_status` tinyint(1) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (23, 9, '2020080616231850', 5110.00, 'long zhang', '15201686411', '北京市海淀区', '10000', 0, 0, 0, 1596702207);
INSERT INTO `order` VALUES (24, 10, '2020080616282984', 2399.00, '', '', '', '', 0, 0, 0, 1596702495);
INSERT INTO `order` VALUES (25, 10, '2020080616336041', 6397.00, 'zhangsan', '15264111521', '北京市海淀区', '10000', 0, 0, 0, 1596702816);
INSERT INTO `order` VALUES (26, 10, '2020080616451576', 1299.00, 'zhangsan', '15264111521', '北京市海淀区', '10000', 0, 0, 0, 1596703554);
INSERT INTO `order` VALUES (27, 10, '2020080616581850', 3511.00, 'zhangsan', '15264111521', '北京市海淀区', '10000', 1, 1, 0, 1596704289);
INSERT INTO `order` VALUES (28, 10, '2020080617001850', 1599.00, 'zhangsan', '15264111521', '北京市海淀区', '10000', 0, 0, 0, 1596704401);
INSERT INTO `order` VALUES (29, 6, '2020081912495688', 3998.00, '赵四', '15264111111', '北京市海淀区', '10000', 1, 0, 1, 1597812582);
INSERT INTO `order` VALUES (30, 6, '2020081916421850', 1112.00, '赵四', '15264111111', '北京市海淀区', '10000', 0, 0, 0, 1597826568);
INSERT INTO `order` VALUES (31, 6, '2020081916595688', 3511.00, '赵四', '15264111111', '北京市海淀区', '10000', 0, 0, 0, 1597827554);
INSERT INTO `order` VALUES (32, 6, '2020081917273784', 3998.00, '赵四', '15264111111', '北京市海淀区', '10000', 0, 0, 0, 1597829231);
INSERT INTO `order` VALUES (33, 6, '2020081917289905', 3511.00, '赵四', '15264111111', '北京市海淀区', '10000', 1, 1, 0, 1597829330);
INSERT INTO `order` VALUES (34, 6, '2020081917331056', 3511.00, '赵四', '15264111111', '北京市海淀区', '10000', 1, 0, 1, 1597829625);
INSERT INTO `order` VALUES (35, 6, '2020081918241850', 1599.00, '赵四', '15264111111', '北京市海淀区', '10000', 1, 0, 1, 1597832685);
INSERT INTO `order` VALUES (36, 6, '2020082017171850', 3998.00, '哈哈', '15201686411', '北京市海淀区111', '10000', 1, NULL, 1, 1597915063);

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `order_id` int(0) NULL DEFAULT NULL,
  `uid` int(0) NULL DEFAULT NULL,
  `product_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `product_id` int(0) NULL DEFAULT NULL,
  `product_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `product_price` decimal(10, 2) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `product_num` int(0) NULL DEFAULT NULL,
  `goods_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES (27, 23, 9, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (28, 23, 9, 'Redmi K30 系列-Redmi K30 4G', 33, 'static/upload/20200717/1594971710634525600.jpg', 1599.00, 0, 1, 'Redmi K30 4G', '蓝色');
INSERT INTO `order_item` VALUES (29, 23, 9, '小米8年度旗舰222', 21, 'static/upload/20200618/1592452490474942700.jpg', 1112.00, 0, 1, '3GB+32GB', '黑色');
INSERT INTO `order_item` VALUES (30, 24, 10, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (31, 25, 10, 'Redmi K30 系列-Redmi K30 4G', 33, 'static/upload/20200717/1594971710634525600.jpg', 1599.00, 0, 1, 'Redmi K30 4G', '蓝色');
INSERT INTO `order_item` VALUES (32, 25, 10, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 2, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (33, 26, 10, '小米9', 19, 'static/upload/20200617/1592392307796676500.jpg', 1299.00, 0, 1, '8GB+256GB', '黑色');
INSERT INTO `order_item` VALUES (34, 27, 10, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (35, 27, 10, '小米8年度旗舰222', 21, 'static/upload/20200618/1592452490474942700.jpg', 1112.00, 0, 1, '3GB+32GB', '黑色');
INSERT INTO `order_item` VALUES (36, 28, 10, 'Redmi K30 系列-Redmi K30 4G', 33, 'static/upload/20200717/1594971710634525600.jpg', 1599.00, 0, 1, 'Redmi K30 4G', '蓝色');
INSERT INTO `order_item` VALUES (37, 29, 6, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (38, 29, 6, 'Redmi K30 系列-Redmi K30 4G', 33, 'static/upload/20200717/1594971710634525600.jpg', 1599.00, 0, 1, 'Redmi K30 4G', '蓝色');
INSERT INTO `order_item` VALUES (39, 30, 6, '小米8年度旗舰222', 21, 'static/upload/20200618/1592452490474942700.jpg', 1112.00, 0, 1, '3GB+32GB', '黑色');
INSERT INTO `order_item` VALUES (40, 31, 6, '小米8年度旗舰222', 21, 'static/upload/20200618/1592452490474942700.jpg', 1112.00, 0, 1, '3GB+32GB', '黑色');
INSERT INTO `order_item` VALUES (41, 31, 6, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (42, 32, 6, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (43, 32, 6, 'Redmi K30 系列-Redmi K30 4G', 33, 'static/upload/20200717/1594971710634525600.jpg', 1599.00, 0, 1, 'Redmi K30 4G', '蓝色');
INSERT INTO `order_item` VALUES (44, 33, 6, '小米8年度旗舰222', 21, 'static/upload/20200618/1592452490474942700.jpg', 1112.00, 0, 1, '3GB+32GB', '黑色');
INSERT INTO `order_item` VALUES (45, 33, 6, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (46, 34, 6, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '蓝色');
INSERT INTO `order_item` VALUES (47, 34, 6, '小米8年度旗舰222', 21, 'static/upload/20200618/1592452490474942700.jpg', 1112.00, 0, 1, '3GB+32GB', '黑色');
INSERT INTO `order_item` VALUES (48, 35, 6, 'Redmi K30 系列-Redmi K30 4G', 33, 'static/upload/20200717/1594971710634525600.jpg', 1599.00, 0, 1, 'Redmi K30 4G', '蓝色');
INSERT INTO `order_item` VALUES (49, 36, 6, 'Redmi K30 系列-Redmi K30 4G', 33, 'static/upload/20200717/1594971710634525600.jpg', 1599.00, 0, 1, 'Redmi K30 4G', '蓝色');
INSERT INTO `order_item` VALUES (50, 36, 6, 'Redmi K30 系列-Redmi K30 5G', 34, 'static/upload/20200717/1594973579432414700.jpg', 2399.00, 0, 1, 'Redmi K30 5G', '薄荷');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (2, '销售部门', '销售部门', 1, 1591061390);
INSERT INTO `role` VALUES (4, '编辑部门', '这是一个编辑部门，主要负责文章编辑', 1, 1591062092);
INSERT INTO `role` VALUES (11, '软件部门', '软件部门', 1, 1591073850);
INSERT INTO `role` VALUES (12, '市场部门', '市场部门', 1, 1591073860);

-- ----------------------------
-- Table structure for role_access
-- ----------------------------
DROP TABLE IF EXISTS `role_access`;
CREATE TABLE `role_access`  (
  `role_id` int(0) NOT NULL,
  `access_id` int(0) NOT NULL,
  PRIMARY KEY (`role_id`, `access_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_access
-- ----------------------------
INSERT INTO `role_access` VALUES (2, 52);
INSERT INTO `role_access` VALUES (2, 54);
INSERT INTO `role_access` VALUES (2, 55);
INSERT INTO `role_access` VALUES (2, 56);
INSERT INTO `role_access` VALUES (2, 57);
INSERT INTO `role_access` VALUES (4, 53);
INSERT INTO `role_access` VALUES (4, 59);
INSERT INTO `role_access` VALUES (4, 60);
INSERT INTO `role_access` VALUES (4, 72);
INSERT INTO `role_access` VALUES (4, 73);
INSERT INTO `role_access` VALUES (11, 52);
INSERT INTO `role_access` VALUES (11, 53);
INSERT INTO `role_access` VALUES (11, 54);
INSERT INTO `role_access` VALUES (11, 55);
INSERT INTO `role_access` VALUES (11, 56);
INSERT INTO `role_access` VALUES (11, 57);
INSERT INTO `role_access` VALUES (11, 59);
INSERT INTO `role_access` VALUES (11, 60);
INSERT INTO `role_access` VALUES (11, 72);
INSERT INTO `role_access` VALUES (11, 78);
INSERT INTO `role_access` VALUES (11, 79);
INSERT INTO `role_access` VALUES (11, 80);
INSERT INTO `role_access` VALUES (11, 81);
INSERT INTO `role_access` VALUES (11, 82);
INSERT INTO `role_access` VALUES (11, 83);
INSERT INTO `role_access` VALUES (12, 52);
INSERT INTO `role_access` VALUES (12, 54);
INSERT INTO `role_access` VALUES (12, 55);
INSERT INTO `role_access` VALUES (12, 56);
INSERT INTO `role_access` VALUES (12, 57);
INSERT INTO `role_access` VALUES (12, 64);
INSERT INTO `role_access` VALUES (12, 65);
INSERT INTO `role_access` VALUES (12, 74);

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `site_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `site_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `site_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `no_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `site_icp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `site_tel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `search_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `tongji_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `appid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `app_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `end_point` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `bucket_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES (1, 'beego仿小米商城项目', 'static/upload/20200627/1593231907138689200.png', '小米', '小米', 'static/upload/20200627/1593234395400009600.jpg', '2422', '24', '小米,电视', '11111', 'GJoqWHXB2c9S9gwP', 'Lgf3weXuWITUUb17vDJfveg1jmKEe9', 'oss-cn-beijing.aliyuncs.com', 'beego');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `last_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `status` tinyint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (5, 'e10adc3949ba59abbe56e057f20f883e', '15201686455', '[', '', 0, 0);
INSERT INTO `user` VALUES (6, 'fcea920f7412b5da7be0cf42b8c93759', '15201686411', '[', '', 0, 0);
INSERT INTO `user` VALUES (7, 'e10adc3949ba59abbe56e057f20f883e', '15201686410', '[', '', 0, 0);
INSERT INTO `user` VALUES (8, 'e10adc3949ba59abbe56e057f20f883e', '15201686444', '[', '', 0, 0);
INSERT INTO `user` VALUES (9, 'e10adc3949ba59abbe56e057f20f883e', '15201686459', '[', '', 0, 0);
INSERT INTO `user` VALUES (10, 'e10adc3949ba59abbe56e057f20f883e', '15201686333', '[', '', 0, 0);

-- ----------------------------
-- Table structure for user_temp
-- ----------------------------
DROP TABLE IF EXISTS `user_temp`;
CREATE TABLE `user_temp`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `send_count` int(0) NULL DEFAULT NULL,
  `add_day` int(0) NULL DEFAULT NULL,
  `add_time` int(0) NULL DEFAULT NULL,
  `sign` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_temp
-- ----------------------------
INSERT INTO `user_temp` VALUES (32, '[', '15201686456', 1, 20200729, 1595999226, 'f0f910de2b6c0130d71237575b425f4b');
INSERT INTO `user_temp` VALUES (33, '[', '15201686457', 2, 20200729, 1595999644, '0e3c6d5d465e018f24dd82eea1101d4c');
INSERT INTO `user_temp` VALUES (34, '[', '15202686410', 1, 20200729, 1596004092, '95c05218e40295531158082b521d2f9d');
INSERT INTO `user_temp` VALUES (35, '[', '15201686555', 1, 20200729, 1596007423, 'e457b6cf770f389a559e29cb2e053bca');
INSERT INTO `user_temp` VALUES (36, '[', '15201686410', 1, 20200729, 1596008681, 'b6bb9359df895b77a800e90f45d70c5b');
INSERT INTO `user_temp` VALUES (37, '[', '15201686444', 1, 20200730, 1596113177, 'feb427d53c75a8e3a39ae39c1cef9c25');
INSERT INTO `user_temp` VALUES (38, '[', '15201686459', 1, 20200806, 1596698877, '1460526a6126646929d50b50a5d9316d');
INSERT INTO `user_temp` VALUES (39, '[', '15201686333', 1, 20200806, 1596702373, 'adad4b93a0b756dc5de0ba3f16346aee');

SET FOREIGN_KEY_CHECKS = 1;
